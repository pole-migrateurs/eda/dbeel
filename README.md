# dbeel

dbeel integration
This part contains script related to dbeel, which integrates dams and electrofishing data

The working branch is currently develop



## Name
Data base for eel dbeel integration script.

## Description
This project contains the scripts to integrate data from various places (Spain, Portugal, France, Spain, Sweden, United Kinddom, Belgium) into the dbeel. Version 2.0 corresponds to the POSE project an further integration of dbeel data with the CCM.
Version 2.1 corresponds to an integration with RHT
Version 2.2 RHT
Vertion 2.3 corresponds to the development in sudoang.

## Support
cedric.briand@eptb-vilaine.fr
mmateo@azti.es


## Project status
dbeel is active, last developements 2022 june, import of electrofishing data France.
