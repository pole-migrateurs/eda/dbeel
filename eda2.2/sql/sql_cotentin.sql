---------------------------------------
-- SCRIPT D'EXPORT DES DONNEES POUR Allan Dufouil, stagiaire fish pass travaillant 
-- dans le cotentin
-- EDA 2.2.1 12/07/2019
------------------------------------------------
/*
Tout d�abord merci pour l�aiguillage vers le site du PONAPOMI. J�ai donc bien r�ussi � t�l�charger les couches EDA, notamment la couche dcdf (densit� anguilles jaunes) et cumul_aval (densit� anguilles argent�es). J�ai extrait les donn�es qui m�int�ressaient et j�ai quelques questions :
-    Pour certains cours d�eau situ�s sur la zone d��tude, on observe des densit�s moyennes en anguilles argent�es nettement sup�rieures aux densit�s d�anguilles jaunes. A titre d�exemple, la Grande Crique pr�sente une densit� de 106 ind. AG /100 m� contre 16 ind. J / 100 m�. La densit� d�anguilles argent�es se base bien sur celle des anguilles jaunes (utilisation du mod�le d�argenture) ?
-    On aimerait �galement avoir des biomasses produites sur ces m�mes axes. Les donn�es ne semblent pas apparaitre sur les couches dcdf et cumul_aval. On a bien les donn�es de densit� en individus pour chacun des tron�ons. Auriez-vous une couche pr�sentant les largeurs et longueurs de ces tron�ons afin de pouvoir �valuer une biomasse en kg/ha.
*/


ALTER TABLE public."Secteur d'�tude" SET SCHEMA temp;
DROP TABLE IF EXISTS temp.result_cotentin2;
CREATE TABLE temp.result_cotentin2 AS (
SELECT vv.*, rr.the_geom FROM rht.rhtvarmod vv 
JOIN rht.rhtvs2 rr ON vv.id_drain=rr.id_drain
JOIN temp."Secteur d'�tude" tt ON st_intersects(st_transform(tt.geom,3035),rr.the_geom))

/*
cd C:\temp\stage_allan_dufouil_fish_pass
pgsql2shp -u postgres -f "result_cotentin2" -u postgres eda2.2 temp.result_cotentin2