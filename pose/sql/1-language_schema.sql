CREATE LANGUAGE plpgsql;

---************************************--
-- 	schemas
---************************************--
CREATE SCHEMA "dbeel_nomenclature";
CREATE SCHEMA "dbeel";
CREATE SCHEMA "onema";