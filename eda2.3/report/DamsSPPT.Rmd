---
title: "Cumulated dams height from the sea - sudoang Spain Portugal database"
author: "Cedric and Maria and Mathilde"
date: "07 november 2019"
output: 
  html_document:
    number_sections: true
---





```{r launch, echo=FALSE, include=FALSE}




require(knitr)
opts_knit$set(eval.after = 'fig.cap')
# this will set figure caption to cap inside R chunk
obj <- ls(all=TRUE)
obj <- obj[!obj%in%c("passworddistant","passwordlocal")]
getUsername <- function(){
	name <- Sys.info()[["user"]]
	return(name)
}
#memory.limit(size =4000)
if(getUsername() == 'cedric.briand'|getUsername() == 'mmateo') setwd("C:/workspace/EDA/")
rivernetwork <- "RIOS"
source("EDACCM/init.r")
#citation("sqldf")
#citation("RPostgreSQL")
#citation("RColorBrewer")
#citation("plyr")
#citation("ggplot2")
#citation("stargazer")
#citation("visreg")
#citation("mgcv")
#citation("PresenceAbsence")
graphics.off() 

#options(warn =  -1)
options(sqldf.RPostgreSQL.user = "postgres", 
		sqldf.RPostgreSQL.password = passwordlocal,
		sqldf.RPostgreSQL.dbname = "eda2.3",
		sqldf.RPostgreSQL.host = "localhost",#  1.100.1.6
		sqldf.RPostgreSQL.port = 5432)
#t <- tryCatch(sqldf("select gid from rht.rhtvs2 limit 1"),error =  function(e) e)
#is_simple_error <- function(x) inherits(x, "simpleError")
#if (is_simple_error(t)) stop("distant call using w3.eptb-vilaine.fr failed, check connection to the database")


#baseODBCccm[1] <- "eda2"
#baseODBC[1] <- "ouvragelb"
#baseODBCdbeel[1] <- "dbeel"
#baseODBCrht[1] <- "eda2rht"

# Attention il faut crï¿½er les rï¿½pertoires
options(warn =  0)
#setwd("C:/workspace/EDA/report2.3")
#datawd <- "C:/workspace/EDAdata/"
ddatawd <- paste0(datawd,"/report2.3/data/")
#imgwd <- "C:/workspace/EDAdata/reportt2.3/images/"
#edawd =  "C:/workspace/EDA/"

sanitizeLatexS <- function(str) {
	gsub('([#$%&~_\\^\\\\{}])', '\\\\\\\\\\1', str, perl = TRUE);
}
# la fonction sn utilise maintenant le package siunitx
# ajouter \usepackage{siunitx}
#\sisetup{
#round-mode = places, % nombre de decimales apres la virgule
#round-precision = 3
#}%
# Dans le preambule
num <- function(x,round_precision =  NULL)
{
	if (is.null(round_precision)) {
		return(sprintf("\\num{%s}", x))
	} else {
		return(sprintf("\\num[round-precision=%s]{%s}",round_precision+1, x))
	}
}
wd <- getwd()
# choix de la version a charger
rapport <- "2020" #"2012" "2015" "2009" 
sector <- "spain"
open_in_excel <- function(some_df){
	tFile <- paste("C:/temp/",gsub("\\\\","",tempfile(fileext =  paste0(substitute(some_df), ".csv"),tmpdir =  "")),sep =  "")
	write.table(some_df, tFile, row.names = F, sep = ";", quote = F)
	system(paste('open -a \"/ProgramData/Microsoft/Windows/Start Menu/Programs/Microsoft Office/Microsoft Excel 2010\"', tFile))
}
library(readxl)
library(sf)
library(scam)
library(colorspace)
# vvv <- list()
# save(vvv, file=paste0(ddatawd,"vvv.Rdata"))
```

The following table describes where most scripts to build dam data are located
and whether it is necessary to launch them to rebuild the database

```{r instructions, echo=FALSE, include=FALSE, results='asis'}
library(kableExtra)
steps <- tribble(~name, ~file, ~description, ~launch_to_rebuild,
	"initial integration", "sudoang/GT2_obstacles.R", "Integration of tables to R", "no",
	"initial integrationSPPT", "EDAcommun/sql/dbeel_sudoang_obstruction_creation.sql", "Integration of tables to R", "no",	
	"French integration","EDAcommun/sql/import_dam_france.sql", "this will allow to rebuild the data from ICE/BDOE/ROE database", "yes (FR)",
	"Views and downstream calculations","EDAcommun/sql/dbeel_view_obstruction.sql",	"These views include the joining of all segments with dam downstream","yes(FR,SP,PT)",
	"French explanation+ model missing height fr","Eda_Rios/Dams.Rmd","this script explains the main work and choices done to import data and then builds the model for missing height","yes (FR)",
	"model missing height sppt+ downstream_cumul (all)","Eda_Rios/DamsSPPT.Rmd","Model missing for Spain and portugal and then calculate cumulated values for all","yes (FRSPPT)"
	)
steps%>% kable(escape = F, align = "c", "html") %>%
		kable_styling(c("striped", "condensed"), full_width = T)%>%
		scroll_box(width = "1000px", height = "800px")

```

# Importing data





## Calculating the distance projection for all segments

The [dbeel_view_obstruction](http://w3.eptb-vilaine.fr:8080/traceda/browser/eda/EDAcommun/sql/sudoang/dbeel_view_obstruction.sql) script creates a view of all dams in the sudoang area
**sudoang.view_obstruction** and creates the projection table for all countries
**france.join_obstruction_rn**, **spain.join_obstruction_rn** and **portugal.join_obstruction_rn**

```{sql projection, eval=FALSE}
DROP TABLE IF EXISTS spain.join_obstruction_rn;
CREATE TABLE spain.join_obstruction_rn as(
  
   with projection as (	
               SELECT r.idsegment, 
               op_id,               						
		geom,
		st_distance(r.geom,d.the_geom) as distance ,
        1-ST_LineLocatePoint(ST_GeometryN(r.geom,1),ST_ClosestPoint(r.geom,d. the_geom)) AS position_ouvrage_segment

		FROM spain.rn r
		JOIn sudoang.dbeel_obstruction_place  d
		ON st_dwithin(r.geom, d.the_geom,0.01)
		order by idsegment, distance desc)
SELECT distinct on (op_id) op_id, idsegment, position_ouvrage_segment  from projection
);
--20606 18s
```


```{r out.width="400px", echo=FALSE, fig.cap = cap}
knitr::include_graphics("C:/workspace/EDAdata/report2.3/images/distance_dam_spain.jpg")
cap <- "Projection of dams on segments with distance from the dowstream part of the segment (view_join_obstruction_rn) "
```

```{r out.width="6500px", echo=FALSE, fig.cap = cap}
knitr::include_graphics("C:/workspace/EDAdata/report2.3/images/downstream_dam_from_segment_PT9621.png")
cap <- "Map showing the dams located downstream from idsegment PT9621 "
```

## Calculating all dams located downstream from one riversegment

The script is located there
[dbeel_view_obstruction](http://w3.eptb-vilaine.fr:8080/traceda/browser/eda/EDAcommun/sql/sudoang/dbeel_view_obstruction.sql)
All dams located downstream from one river segment are associated with this
river segment. This functions uses the path from the sea (a ltree which is
postgres extension allowing to store hierarchical types) collect all the
segments downstream from one segment. It uses also the 

```{sql projection_downstream, eval=FALSE}
# Portugal

DROP TABLE IF EXISTS portugal.join_obstruction_rn_downstream; CREATE TABLE
portugal.join_obstruction_rn_downstream as( WITH downstreamjoin AS (  
  select unnest(regexp_split_to_array(ltree2text(path),E'\\.+')) as
vecteurchemin,   rn.idsegment AS idsegmentsource  
  from portugal.rn )  SELECT j.op_id,  j.position_ouvrage_segment, 
 downstreamjoin.* FROm downstreamjoin  JOIN portugal.join_obstruction_rn j ON
j.idsegment=downstreamjoin.vecteurchemin ); --217489 rows affected, 02:18
minutes execution time. SELECT * FROM portugal.join_obstruction_rn_downstream
where idsegmentsource='PT9621' ;

# dbeel_rivers

DROP TABLE IF EXISTS dbeel_rivers.join_obstruction_rn_downstream; CREATE TABLE
dbeel_rivers.join_obstruction_rn_downstream as( WITH downstreamjoin AS (  
  select unnest(regexp_split_to_array(ltree2text(path),E'\\.+')) as
vecteurchemin,   rn.idsegment AS idsegmentsource  
  from dbeel_rivers..rn )  SELECT j.op_id,  j.position_ouvrage_segment, 
 downstreamjoin.* FROm downstreamjoin  JOIN portugal.join_obstruction_rn j ON
j.idsegment=downstreamjoin.vecteurchemin


# spain

DROP TABLE IF EXISTS spain.join_obstruction_rn_downstream; CREATE TABLE
spain.join_obstruction_rn_downstream as( WITH downstreamjoin AS (  
  select unnest(regexp_split_to_array(ltree2text(path),E'\\.+')) as
vecteurchemin,   rn.idsegment AS idsegmentsource  
  from france.rn )  SELECT j.op_id,  j.position_ouvrage_segment,  downstreamjoin.*
FROm downstreamjoin  JOIN france.join_obstruction_rn j ON
j.idsegment=downstreamjoin.vecteurchemin );
```

The table joining segments and dams is formatted as following :


<html><head><style>table {border: medium solid #6495ed;border-collapse: collapse;width: 100%;} th{font-family: monospace;border: thin solid #6495ed;padding: 5px;background-color: #D0E3FA;background-image: url(sky.jpg);}td{font-family: sans-serif;border: thin solid #6495ed;padding: 5px;text-align: center;background-color: #ffffff;}.odd{background:#e8edff;}img{padding:5px; border:solid; border-color: #dddddd #aaaaaa #aaaaaa #dddddd; border-width: 1px 2px 2px 1px; background-color:white;}</style></head><body><table><tr><th colspan="4">select * from portugal.join_obstruction_rn_downstream limit 10</th></tr><tr><th>op_id</th><th>position_ouvrage_segment</th><th>vecteurchemin</th><th>idsegmentsource</th></tr><tr class="odd"><td>e36567c1-46bf-40a3-956e-f009a7887950</td><td>0,8388772288</td><td>PT27939</td><td>PT95884</td></tr>
<tr><td>5d32529c-33f2-43c7-b3b0-03fcc488227a</td><td>0,7060498951</td><td>PT94869</td><td>PT95900</td></tr>
<tr class="odd"><td>dd503433-3284-467a-851e-0ebdb142b3c9</td><td>0,1534516105</td><td>PT48634</td><td>PT9621</td></tr>
<tr><td>5fd89d5e-97a2-47b2-a910-cdcff188558b</td><td>0,1919817742</td><td>PT9832</td><td>PT9621</td></tr>
<tr class="odd"><td>5ff7bd6c-bee2-4a26-8158-80160ba96844</td><td>0,7741356929</td><td>PT9766</td><td>PT9621</td></tr>
<tr><td>4cc6bfd8-7cc6-482a-be68-d644f74a14a8</td><td>0,4381541402</td><td>PT48211</td><td>PT9621</td></tr>
<tr class="odd"><td>f4b30616-2197-41af-8d83-fc6cb571b54c</td><td>0,7961669318</td><td>PT47947</td><td>PT9621</td></tr>
<tr><td>e926497e-6b41-4141-a7c1-8a874e0587ab</td><td>0,5166282925</td><td>PT47948</td><td>PT9621</td></tr>
<tr class="odd"><td>aea98208-fd9c-4e47-9ec8-7c26d278b90a</td><td>0,3891534331</td><td>PT47627</td><td>PT9621</td></tr>
<tr><td>dfd0c148-4a11-4cff-bf3b-d997bbf554b9</td><td>0,5840848276</td><td>PT47716</td><td>PT9621</td></tr>
</table></body></html>



# Calculation of missing values for Spain and Portugal

A common model will be built for both Spain and Portugal.

```{r loaddatadamheightspainportugal, echo=TRUE, eval=TRUE}


# we load the join table view_obstruction from EDA class
dam_rios_all <- new("BaseEdaRiosRiversegmentsDam", 
		baseODBC = "baseODBCrios", # must be set in init length 1
		schema = "dbeel_rivers",
		table = "rn",
		join_obstruction_rn_downstream= "dbeel_rivers.join_obstruction_rn_downstream",
		joinschema = "sudoang",
		jointable = "view_obstruction",
		zonegeo = c("Spain","Portugal"),
		basin=as.character(NULL),
		prkey = "idsegment")

dam_rios_all <- loaddb2(dam_rios_all) 
#Calling loadb method for Spain   
#Attention use loaddb() with cumulated_dam_impact (the version using routing from downstream) and loaddb2() with cumulated_dam_impact2  (the version using the join_obstruction_rn_downstream big big table)
#join_obstruction_rn_downstream  loaded 6198530 lines
#time for query on dams (mins) = 1.43  
#dam data loaded: 20589 lines 
#time for query on dams (mins) = 0.01  
#time for query (mins) 1.2data from the riversegments loaded: 400458 lines 
#time for query on rivers (mins) = 1.24  
# Final join_downstream_obstruction_rn size 11102912 
# debug
# object <- dam_rios_spain 

#dam_rios_spain <- loadshp(dam_rios_spain)
save(dam_rios_all, file=str_c(ddatawd,"dam_rios_all.Rdata"))
##############################
# Predicting missing values RUNONCE (this will work for France only, adapt another model to spain and portugal
##############################

# load(file=str_c(ddatawd,"dam_rios_all.Rdata"))
require(dplyr)
dams <- dam_rios_all@datadam # 21462

join_dam_rn <- sqldf("select * from dbeel_rivers.join_obstruction_rn") # 205685

dd <- dplyr::inner_join(join_dam_rn,dams, by="op_id") # 21414
rr <- dplyr::inner_join(dd,dam_rios_all@data, by="idsegment") # 21413
# table joining 

rr<- rename(rr,"h"="po_obstruction_height")
save(rr,file=str_c(ddatawd,"rrall.Rdata"))
rm(dam_rios_all)
gc()


```



The dams with the largest height have been analysed. The dams corresponding to
penstock pipes have been attributed the new code PP as these often correspond to
very large height. Among the dams with large height some correspond to very
small dams introduced by error. 

```{sql correct_dams, eval=FALSE}

-- PENSTOCK PIPE 
update sudoang.dbeel_physical_obstruction set ot_no_obstruction_type = 300 where 
	ob_op_id in ('201aeb02-ce62-44a0-9d70-6226a96e1193','7fd1175d-633c-43b9-875e-d502c899f587','3fa241ce-6f23-43fb-8b1a-6be6e5aafdb4',
	'18fa1efc-4e1f-409a-91c7-73b363fd120d','cf81fe1a-16d7-4914-849a-3802e51a30ec','98ecc950-930a-4f12-8448-ec4ff9d9cbf9','24212856-af6d-4527-8197-933bc982c4c6',
	'7cd3dd53-f0bf-4995-9be6-beb25c0696b6','c4bc3cc2-b94b-4fc7-8ea5-cd8b808f55c8','3e0edf36-b3a3-4720-8e1c-095b9e6b6283','5d3667d8-8f38-4c03-a3d3-ed9735383ee2',
	'44743bf9-7098-48f4-890d-136044c5ceba','97c37195-a41a-4da7-ab4e-15d61238a318','52e54dc1-465f-494f-93d3-ad3e7f3837aa','8bbc4610-d8da-4160-ace6-fbac6e3ca2c0',
	'81cc0962-19e8-4dbc-be10-6068f7df6c96','e3c2c7a0-36bd-42fa-be60-bd2e792eed69','3dd6b5bd-bb57-46d4-b004-eaa059a2c04a','29a6e540-85d4-4f25-8806-a8632c56b7e9',
	'e4367ce4-ff80-4063-9673-d64fc74429bf','5a9e3492-eacd-4759-87fe-0cf59cb06e54','d696d253-4647-4d49-b730-8f0ab9bb852e','66bef3e3-65ee-46b4-96ab-788bb40337c1',
	'ebf3290f-13ee-443e-a985-22ae9fbf46bb','4b136fab-ab1a-488a-b0da-65bb380cb2bf','53e54958-9c59-4a4e-83a9-694d06827def','028c14a4-f273-44f8-8cc8-ec4eb0c2cfc6',
	'b44ac6f7-2fa6-45b4-b134-45d452617d0f','a5a2ac9e-f9df-4215-8df4-bf3f29041c77'
	); -- 29 rows

update sudoang.dbeel_physical_obstruction set po_obstruction_height = 0 where
	ob_op_id in ('17df2639-af36-4df5-85e5-87efcd3afce2','3ec6f823-93a5-48e8-af1c-6bdb91c4440e','883dece6-96eb-4364-bb1e-ea16b91b8211','5560fae3-ac96-48fc-9327-0c6b7659db87',
	'a82f9be4-3e6a-4ad5-b543-462eb1a792b5','d94c301a-84bf-4156-9728-44a873a13b80','54243000-1d47-4d9c-8465-50bf807975ae','3fcd953e-e5c1-4073-97fb-587b1ff59282'
	); -- 8 rows

-- PARTICULAR CASES
update sudoang.dbeel_physical_obstruction set po_obstruction_height = 11.5 where ob_op_id = '080d5c7c-1a93-40af-870b-c27c618925f3';
update sudoang.dbeel_physical_obstruction set po_obstruction_height = 21 where ob_op_id = 'f3a17509-c063-4cf7-b377-539077bdb120';
update sudoang.dbeel_physical_obstruction set po_obstruction_height = 89 where ob_op_id = '1032b512-5426-41ab-b841-00d001434bac';--b0


```

```{r modeldamheightall, echo=TRUE, eval=TRUE}



load(file=str_c(ddatawd,"rrall.Rdata")) # named rr
# recodage du jeu de données
rr%>%select(basin)%>%distinct()%>%kable()
describe(rr)%>%Hmisc::html()
tobig <- rr %>% select(op_id,ob_op_id,dp_name, obstruction_type_code, h, googlemapscoods, id_original, country) %>% arrange(desc(h)) %>% head(200)
# plot(st_geometry(dam_rios_spain@shpdam))
#tooobig <- merge(dam_rios_spain@shpdam,tobig, by="op_id")
#plot(st_geometry(tooobig))



#tobig4 <- rr %>% select(op_id,ob_op_id,dp_name, obstruction_type_code, h, googlemapscoods, id_original) %>% arrange(desc(h)) %>% head(150)
#ex(tobig4)
#TODO ANALYSIS OF LARGEST HEIGHT. A NEW TYPE 'PP' HAS BEEN INCLUDED
sum(rr$h>100 & rr$obstruction_type_code!="PP", na.rm=TRUE) # 51


rr %>% filter(obstruction_type_code %in%c("BR","CU"))%>%ggplot()+ stat_density(aes(x=h))
rr %>% filter(obstruction_type_code %in%c("BR","CU"))%>%summarize(median(h, na.rm=TRUE)) # median 0
rr %>% filter(obstruction_type_code %in%c("BR","CU"))%>%summarize(mean(h, na.rm=TRUE)) # mean 0.097
# we will set height to zero for missing data for this type




summary(rr$slope)
rr$slope[is.na(rr$slope)] <- median(rr$slope, na.rm=TRUE)
rr$ls <- log(rr$surfacebvm2+1)
rr$sqrtslope <- sqrt(rr$slope+0.01)
rrm <- rr %>% filter(!obstruction_type_code %in%c("PP","CU","BR","FO") &
				h<250&!is.na(rr$h)&rr$h>0)  
layout(matrix(c(1,2), nrow = 2, byrow = TRUE))
hist(rr$ls,100)
hist(rrm$ls,100)

layout(matrix(c(1,2), nrow = 2, byrow = TRUE))
hist(rr$sqrtslope,100,main="prediction datasets")
hist(rrm$sqrtslope,100, main ="model dataset")

nrow(rrm)


ggplot(rrm)+geom_point(aes(x=sqrt(slope+0.01),y=log(h)))+
		geom_smooth(aes(x=sqrt(slope+0.01),y=log(h)),method = "lm", color="purple")+
		facet_wrap(~ obstruction_type_code)+
		ggtitle("Trend between terrain slope and dam height according to dam type")



# too many missing data
ggplot(rrm)+geom_point(aes(x=log(dis_m3_pyr_riveratlas+1),y=log(h)))+
		geom_smooth(aes(x=log(dis_m3_pyr_riveratlas+1),y=log(h)),method = "lm", color="green")+
		facet_wrap(~ obstruction_type_code)+
		ggtitle("Trend between flow (source riverAtlas) and dam height according to dam type")

ggplot(rr)+geom_point(aes(x=log(surfacebvm2+1), y=log(dis_m3_pyr_riveratlas+1), col=basin))

ggplot(rr[log(rr$surfacebvm2+1)>1,])+geom_point(aes(x=log(surfacebvm2+1), y=log(dis_m3_pyr_riveratlas+1), col=basin))

ggplot(rrm)+geom_point(aes(x=log(surfacebvm2+1),y=log(h)))+
		geom_smooth(aes(x=log(surfacebvm2+1),y=log(h)),method = "lm", color="green")+
		facet_wrap(~ obstruction_type_code)+
		ggtitle("Trend between surface bassin upstream and dam height according to dam type")

hist(log(rrm$surfacebvm2),50)
hist(log(rr$surfacebvm2),50)
ggplot(rrm)+geom_boxplot(aes(x=basin,y=log(h),fill=basin))+
		scale_fill_discrete_qualitative(palette = "Warm")+
		facet_wrap(~ obstruction_type_code)+
		ggtitle("Trend between dam height and basin according to dam type")


##########################
# GLM de prédiction des hauteurs d'ouvrages
###########################

# les réponses sont linéaires après transformation log


densityplot(log(rrm$h)) 

gg<-glm(log(h)~ls*obstruction_type_code,data=rrm)
# diagnostic du modèle
summary(gg)
# toutes les variables sont significatives
# voir xtable dans le chunk qui suit
#anova(gg,test="Chisq")

###########################
# Diagnostics du GLM
###########################

pdev<-function(mod){
	(mod$null.deviance-mod$deviance)/mod$null.deviance
}
pdev(gg) # 24.1 (29.6 eda2.3 France, 24 eda2.1)
plot(gg,ask=FALSE)

eee <-expand.grid(ls=seq(0,25,by=1),
		obstruction_type_code=unique(rrm$obstruction_type_code),
		sqrtslope=1)
eee$p <-exp(predict(gg, newdata=eee,  se.fit = TRUE)$fit)
eee$pmin <- eee$p - exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)
eee$pmax <- eee$p + exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)

require(ggthemes)
#ggplot(eee)+ 
#		geom_line(aes(x=ls,y=p , col= obstruction_type_code) )+
#		geom_ribbon(aes(x=ls,ymin=pmin, ymax=pmax, fill=obstruction_type_code), alpha=0.2)+
#		ylab("predicted height for and log(slope)=1")+
#		xlab("log( basin surface) with basin surface in m2 ")+
#		scale_fill_tableau()+
#		scale_color_tableau()


eee <-expand.grid(ls=10,
		obstruction_type_code=unique(rrm$obstruction_type_code),
		sqrtslope=seq(0,1.5,by=0.1))
eee$slope <-(eee$sqrtslope)^2-0.01

eee$p <-exp(predict(gg, newdata=eee,  se.fit = TRUE)$fit)
eee$pmin <- eee$p - exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)
eee$pmax <- eee$p + exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)
ggplot(eee)+ 
		geom_line(aes(x=slope,y=p , col= obstruction_type_code) )+
		geom_ribbon(aes(x=slope,ymin=pmin, ymax=pmax, fill=obstruction_type_code), alpha=0.2)+
		ylab("predicted height for log(surface)=10")+
		xlab("slope")+
		scale_fill_tableau()+
		scale_color_tableau()


save(gg, file=str_c(ddatawd,"gg_glm_hauteur_obstacle_spain.Rdata"))


#####################"
# GLMER to account for difference variance per dam type
#####################
require(nlme)

rrm$obstruction_type_code[rrm$obstruction_type_code=='WE'] <- 'PU'
rrm$obstruction_type_code <- as.factor(rrm$obstruction_type_code)
rrm$country <- as.factor(rrm$country)
VS <- varIdent (form = ~ 1 | obstruction_type_code)
ggl<-gls(log(h)~ls*obstruction_type_code,data=rrm,
		weights=VS)
gg0<-gls(log(h)~ls*obstruction_type_code,data=rrm)

anova(gg0,ggl,type="Chisq")
summary(ggl)
rrm$E <-resid(gg)
plot(ggl)
qqnorm(ggl, abline=c(0,1))

eee <-expand.grid(ls=seq(0,25,by=1),
		obstruction_type_code=unique(rrm$obstruction_type_code))
eee$p <-exp(predict(ggl, newdata=eee))


require(ggthemes)
ggplot(eee)+ 
		geom_line(aes(x=ls,y=p , col= obstruction_type_code) )+
		ylab("predicted height for and log(slope)=1, model ggl")+
		xlab("log( basin surface) with basin surface in m2 ")+
		scale_fill_tableau()+
		scale_color_tableau()


#eee <-expand.grid(ls=10,
#		obstruction_type_code=unique(rrm$obstruction_type_code),
#		sqrtslope=seq(0,1.5,by=0.1))
#eee$slope <-(eee$sqrtslope)^2-0.01
#
#eee$p <-exp(predict(gg, newdata=eee,  se.fit = TRUE)$fit)
#eee$pmin <- eee$p - exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)
#eee$pmax <- eee$p + exp(1.96*predict(gg, newdata=eee,  se.fit = TRUE)$se.fit)
#ggplot(eee)+ 
#		geom_line(aes(x=slope,y=p , col= obstruction_type_code) )+
#		geom_ribbon(aes(x=slope,ymin=pmin, ymax=pmax, fill=obstruction_type_code), alpha=0.2)+
#		ylab("predicted height for log(surface)=10")+
#		xlab("slope")+
#		scale_fill_tableau()+
#		scale_color_tableau()

save(ggl, file=str_c(ddatawd,"ggl_gls_hauteur_obstacle_spain.Rdata"))
```



```{r tableglmresults, results="asis", echo=FALSE, eval=TRUE}
save(file=str_c(ddatawd,"ggl_gls_hauteur_obstacle_spain.Rdata"))
xtable::print.xtable(xtable::xtable(anova(ggl,test="F"),
				caption="Anova du modèle des hauteurs"),
				caption.placement="top",
type='html'
)

```

Dams height are predicted according to the previous model but dams where flow (origin river Atlas) > 150 m3/S

```{r  predictmodeldamheight, echo=TRUE, eval=TRUE}

# the predicted hpred include both predicted and observed values
layout(1)
rr$hpred<-rr$h
index_put_to_zero_height <- is.na(rr$h)&rr$obstruction_type_code%in%c("PP","CU","BR","FO","RR")
rr$hpred[index_put_to_zero_height] <- 0
rr$obstruction_type_code <- as.character(rr$obstruction_type_code) # get rid of levels

# checks
#rr$sqrtslope <- pmin(rr$sqrtslope,max(rrm$sqrtslope))
#rr$ls <- pmin(rr$ls,max(rrm$ls))

rr$is_height_predicted<-FALSE
index_replace_missing <- is.na(rr$h)&!rr$obstruction_type_code%in%c("PP","CU","BR","FO","RR")
#---------------------------
# Predictions for glm
#---------------------------
# rr[,c("ls","sqrtslope","obstruction_type_code")][!complete.cases(rr[,c("ls","sqrtslope","obstruction_type_code")]),]
# rr$pred[index_replace_missing] <- predict(ggl, newdata=rr)[index_replace_missing]
# about 15 lines with missing data for log slope

rr$pred[index_replace_missing] <- predict(ggl, newdata=rr[index_replace_missing,])
rr$is_height_predicted[index_replace_missing]<-TRUE # 28340

rr$hpred[index_replace_missing] <- exp(rr$pred[index_replace_missing]) # we have to put the predictions back to exponential


ggplot(rr[rr$obstruction_type_code!="PP",])+ geom_point(aes(x=gid,y=hpred,col=is_height_predicted))+ facet_wrap(~ is_height_predicted, scale='free_y')

# we don't trust the type and don't want add dams that we don't know
hist(rr$hpred[rr$is_height_predicted],500)

# 
ggplot(rr)+geom_histogram(aes(x=log(hpred),fill=obstruction_type_code), position="dodge") + 
		facet_wrap(~ is_height_predicted)

# note this will be loaded later in the class method BaseEdaRiosriversegmentsDam
rrp<-rr[,c("id_original","is_height_predicted","hpred","country")] 
save(rrp,file=str_c(ddatawd,"rrp_spain_portugal.Rdata"))

```

# Cumulated dam impact

The next step is to calculate the cumulated number of dams, this is done in R
in the class  `BaseEdaRiosRiversegmentsDam`   and method
`cumulated_dam_impact2`. The test of this method is done in `analyseRiosDams.R`.


The cumulated dam impact method takes the table joining each riversegment with all dams downstream, calculates tranformation as following :

 * Power tranformation of height, depending on the power (<1 or >1) values (>1 and <1 respectively) are 
 unchanged, the other are power transformed (this is done so as not diminishing the height of low dams (<1 m)
  by power transforming them).
 
 * The score may be taken into account, dams with score pattern matching the vector chosen for dam  
 passability have height set to zero.
 
 * The presence of an eel pass can similarly be used to set the dam height to
   NA, later on we have to set the right coefficient during the calibration
 
 * The number of dams is calculated, except for bridges which are not counted
 
 * The function can also calculate separately cumsums only for dams with pass,
   or dams without pass, and the same with score to try to help with the
   calibration, by passing the two cumulated sums separately in the model.
   
 * The function can also calculate separately cumusums for one country (by setting the data from other countries to NA)

The function takes either a raw value or a value where the missing height are modelled such as in the previous chunk.
The values are then grouped by idsegmentsource and cumulated. Some counts are also created.


```{r loaddatadamheightfrsppt, echo=TRUE, eval=TRUE}


# we load the join table view_obstruction from EDA class
dam_rios_frsppt <- new("BaseEdaRiosRiversegmentsDam", 
		baseODBC = "baseODBCrios", # must be set in init length 1
		schema = "dbeel_rivers",
		table = "rn",
		join_obstruction_rn_downstream= "dbeel_rivers.join_obstruction_rn_downstream",
		joinschema = "sudoang",
		jointable = "view_obstruction",
		zonegeo = c("Spain","Portugal","France"),
		basin=as.character(NULL),
		prkey = "idsegment")
dam_rios_frsppt <- loaddb2(dam_rios_frsppt)
#Calling loadb method for Spain   
#Calling loadb method for Portugal   
#Calling loadb method for France   
#Attention use loaddb() with cumulated_dam_impact (the version using routing from downstream) and loaddb2() with cumulated_dam_impact2  (the version using the join_obstruction_rn_downstream big big table)
#join_obstruction_rn_downstream  loaded 17308074 lines
#time for query on dams (mins) = 1.34  
#dam data loaded: 105734 lines 
#time for query on dams (mins) = 0.06  
#time for query (mins) 1.3data from the riversegments loaded: 516001 lines 
#time for query on rivers (mins) = 1.35  
#Final join_downstream_obstruction_rn size 17308074  

# debug
# object <- dam_rios_spain 
#filter to reduce size
dam_rios_frsppt@datadam <- dam_rios_frsppt@datadam[,c("op_id",  "op_op_id", "id_original",
		 "obstruction_impact_code","obstruction_type_code","po_obstruction_height", 
		 "po_presence_eel_pass", 
		 "fishway_type_code", "fishway_type_name")]

#dam_rios_spain <- loadshp(dam_rios_spain)
save(dam_rios_frsppt, file=str_c(ddatawd,"dam_rios_frsppt.Rdata"))


# load(file=str_c(ddatawd,"dam_rios_frsppt.Rdata"))
require(dplyr)
dams <- dam_rios_frsppt@datadam # 20589





join_dam_rn <- sqldf("select * from dbeel_rivers.join_obstruction_rn") # 20606

dd <- dplyr::inner_join(join_dam_rn,dams, by="op_id") # 20589
rr <- dplyr::inner_join(dd,dam_rios_frsppt@data, by="idsegment") # 20589
# table joining 

rr<- rename(rr,"h"="po_obstruction_height")

# remove dams from main course that are wrongly projected




save(rr,file=str_c(ddatawd,"rrfrsppt.Rdata"))
#rm(dam_rios_frsppt)
#gc()


```

The predictions for missing dam height in France and Spain + Portugal are
merged together to form a full dataset of dam height

```{r  calculating_cumulated_height, echo=TRUE, eval=TRUE}
ddatawd <- paste0(datawd,"/report2.3/data/")

# see DamsSPPT.Rmd
load(file=str_c(ddatawd,"rrp_spain_portugal.Rdata")) # ggl glm model
rrp_sppt <- rrp


# see Dams.Rmd
load(file=str_c(ddatawd,"rrp.Rdata"))
rrp_fr <- rrp
rrp_fr[rrp_fr$id_original=='ROE45139',]
# remove height from downstream water course where dams may have been projected by error on the RHT
bdp<- sqldf("select * from sudoang.downstream_badly_projected")
rrp_fr[rrp_fr$id_original%in%bdp$id_original,'hpred']<-0
rrp_fr$country <- "FR"


rrp <- rbind(rrp_sppt, rrp_fr)
rm(rrp_fr,rrp_sppt)



load(file = str_c(ddatawd,"dam_rios_frsppt.Rdata"))
dam_rios_frsppt <- cumulated_dam_impact2(object=dam_rios_frsppt, predicted_height=rrp)


save(dam_rios_frsppt,file=str_c(ddatawd,"dam_rios_frsppt.Rdata"))
dam_rios_frsppt_data <- dam_rios_frsppt@data
save(dam_rios_frsppt_data,file=str_c(ddatawd,"dam_rios_frsppt_data.Rdata"))
rm(dam_rios_frsppt)


```




```{r  cumulated_height_graphs, echo=TRUE, eval=FALSE}


load(file=str_c(ddatawd,"dam_rios_frsppt_data.Rdata"))
# comparing the values predicted for predicted + pass set to zero + score set to zero
test = tidyr::pivot_longer(dam_rios_frsppt_data, 
		cols=tidyr::ends_with("pps"),
		names_to = c("cs_heightpps"),
		values_to = "h",
		values_drop_na = TRUE)
x11()
ggplot(test)+geom_point(aes(x=distanceseam,y=h,color=cs_heightpps))


# sea idsegments for Gave de Pau, FR118671



test = tidyr::pivot_longer(dam_rios_frsppt_data, 
		cols=c("cs_height_10_n","cs_height_10_p","cs_height_10_pp","cs_height_10_pps"),
		names_to = c("cs_height10"),
		values_to = "h",
		values_drop_na = TRUE)
ggplot(test)+geom_point(aes(x=distanceseam,y=h,color=cs_height10))



```

The database will be udpated in two steps, the first will be to write a
spain.rnadam table with all the variables insides. The variable cumnbdamp
correspond to the number of dams whose height included predicted is different
from zero, the variable cumnbdamso corresponds to the sum of heigth values
different from zero

```{r  write_database, echo=TRUE, eval=TRUE}
# last run january 2021
load(file=str_c(ddatawd,"dam_rios_frsppt_data.Rdata"))
dam_rios_frsppt_data <- dam_rios_frsppt_data %>% rename(cumnbdamso=cumnbdam.y)




columns=c("cumnbdamso",          
		"cs_height_10_n","cs_height_08_n","cs_height_12_n","cs_height_15_n","cumnbdamp","cs_height_10_p",       
		"cs_height_08_p", "cs_height_12_p",  "cs_height_15_p", "cs_height_10_pp", "cs_height_08_pp",  "cs_height_12_pp",      
		"cs_height_15_pp", "cs_height_10_pps", "cs_height_08_pps", "cs_height_12_pps", "cs_height_15_pps", "cs_height_10_pass0",   
		"cs_height_10_pass1","cs_height_10_score0","cs_height_10_score1","cs_height_10_FR",
		"cs_height_10_SP","cs_height_10_PT")
dam_rios_frsppt_data<-dam_rios_frsppt_data[,-grep(".x",colnames(dam_rios_frsppt_data))]
# colnames(dam_rios_frsppt_data)<-gsub('.y','',colnames(dam_rios_frsppt_data))

datadam <-dam_rios_frsppt_data[,c("idsegment",columns)]
sqldf("DROP TABLE IF EXISTS dbeel_rivers.rnadam")
sqldf("create table dbeel_rivers.rnadam as select * from datadam")
sqldf("SELECT AddGeometryColumn('dbeel_rivers','rnadam','geom','3035','MultiLineString',2,TRUE)")
sqldf("COMMENT ON TABLE dbeel_rivers.rnadam IS 'Table updated with latest data in january 2021'")
sqldf("update dbeel_rivers.rnadam set geom=rn.geom from dbeel_rivers.rn where rn.idsegment=rnadam.idsegment")
#Query returned successfully: 516001 rows affected, 02:31 minutes execution time.



sqldf("UPDATE spain.rna set (cumheightdam,cumnbdam)=(NULL,NULL)")
sqldf("UPDATE spain.rna set (cumheightdam,cumnbdam)=(cs_height_10_n,cumnbdamp) from dbeel_rivers.rnadam where rnadam.idsegment=rna.idsegment")



sqldf("UPDATE france.rna set (cumheightdam,cumnbdam)=(NULL,NULL)")
sqldf("UPDATE france.rna set (cumheightdam,cumnbdam)=(cs_height_10_n,cumnbdamp) from dbeel_rivers.rnadam where rnadam.idsegment=rna.idsegment")
sqldf("UPDATE portugal.rna set (cumheightdam,cumnbdam)=(NULL,NULL)")
sqldf("UPDATE portugal.rna set (cumheightdam,cumnbdam)=(cs_height_10_n,cumnbdamp) from dbeel_rivers.rnadam where rnadam.idsegment=rna.idsegment")

# correct cumulated values where no dam

sqldf("UPDATE spain.rna set cumheightdam=0 FROM spain.rn
WHERE rn.idsegment=rna.idsegment
AND cumheightdam IS NULL
AND isendoreic=FALSE;")


sqldf("UPDATE spain.rna set cumnbdam=0 FROM spain.rn
WHERE rn.idsegment=rna.idsegment
AND cumnbdam IS NULL
AND isendoreic=FALSE;") 

sqldf("UPDATE france.rna set cumheightdam=0 FROM france.rn
WHERE rn.idsegment=rna.idsegment
AND cumheightdam IS NULL
AND isendoreic=FALSE;")


sqldf("UPDATE france.rna set cumnbdam=0 FROM france.rn
WHERE rn.idsegment=rna.idsegment
AND cumnbdam IS NULL
AND isendoreic=FALSE;") 

sqldf("UPDATE portugal.rna set cumheightdam=0 FROM portugal.rn
WHERE rn.idsegment=rna.idsegment
AND cumheightdam IS NULL
AND isendoreic=FALSE;")


sqldf("UPDATE portugal.rna set cumnbdam=0 FROM portugal.rn
WHERE rn.idsegment=rna.idsegment
AND cumnbdam IS NULL
AND isendoreic=FALSE;")



```









